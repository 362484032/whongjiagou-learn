const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const  ModuleConcatenationPlugin  = require('webpack/lib/optimize/ModuleConcatenationPlugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
module.exports = {
    // entry: './src/lazy.js',
    entry: {
        // index: './src/index.js',
        // main: './src/main.js',
        // pageA: './src/pageA.js',//提取公共模块
        // pageB: './src/pageB.js',
        // pageC: './src/pageC.js',
        main: './src/host.js',//host scoping
    },
    //chunkFileName chunkash
    output: {
        // path: path.resolve(__dirname, 'dist'),  //模块解析的绝对路径
        path: path.join(__dirname, 'dist'), //目录的绝对路径 __dirname
        filename: '[name].[hash:8].js',   //chunk文件
    },
    devServer:{
        open:true,
        inline:true,//在打包后文件里注入一个websocket客户端
        hot:true//启动模块热加载
    },
    optimization: {
        //提取公共代码(组件模块和库)，方便缓存
		splitChunks: {
			cacheGroups: {
          //不同页面之间的公共模块 library（框架层、业务层）
				commons: {
					chunks: "initial",
					minChunks: 2,
					maxInitialRequests: 5, // The default limit is too small to showcase the effect
					minSize: 0 // This is example is too small to create commons chunks
				},
				vendor: {
					test: /node_modules/,
					chunks: "initial",
					name: "vendor",
					priority: 10,
					enforce: true
				}
			}
		}
    },
    optimization: {
		splitChunks: {
			cacheGroups: {
				commons: {
					chunks: "initial",
					minChunks: 2,
					maxInitialRequests: 5, // The default limit is too small to showcase the effect
					minSize: 0 // This is example is too small to create commons chunks
				},
				vendor: {
					test: /node_modules/,
					chunks: "initial",
					name: "vendor",
					priority: 10,
					enforce: true
				}
			}
		}
	},
    module:{
        rules:[
            {
                test:/\.js$/,
                use:{
                    loader:'babel-loader',
                    options:{
                        presets:[
                            ["env",{modules:false}]
                        ]
                    }
                },
                include:path.resolve('./src'),
                exclude:/node_modules/
            },
            {
                test:/\.css$/,
                loader:['style-loader','css-loader']
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template:'./src/index.html'
        }),
        //开启变量提升 Scope Hoisting
        new ModuleConcatenationPlugin(),  //处理内敛模块
        new webpack.HotModuleReplacementPlugin(),   //模块热更新插件
        new webpack.NamedModulesPlugin(),   //用名称代替ID
        // new CleanWebpackPlugin({
        //     cleanOnceBeforeBuildPatterns: [path.resolve(__dirname, 'dist')],
        // }),
    ]
}