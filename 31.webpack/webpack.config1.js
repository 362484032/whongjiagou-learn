const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const  ModuleConcatenationPlugin  = require('webpack/lib/optimize/ModuleConcatenationPlugin');
module.exports = {
    entry: './src/lazy.js',
    // entry: {
    //     pageA: './src/pageA.js',
    //     pageB: './src/pageB.js',
    //     pageC: './src/pageC.js',
    // },
    //以前没有名字的chunkFileName  chunkhash (hash:20)
    output: {
        // path: path.resolve('dist'),  //模块解析的绝对路径
        path: path.join(__dirname, 'dist'), //目录的绝对路径 __dirname
        filename: '[name].[hash:8].js',   //chunk文件
    },
    devServer:{
        open:true,
        inline:true,//在打包后文件里注入一个websocket客户端
        hot:true//启动模块热加载
    },
    optimization: {
        //提取公共代码(组件模块和库)，方便缓存
		splitChunks: {
			cacheGroups: {
                //不同页面之间的公共模块 library（框架层、业务层）
				commons: {
					chunks: "initial",
					minChunks: 2, 
					maxInitialRequests: 5, // The default limit is too small to showcase the effect
					minSize: 0 // This is example is too small to create commons chunks
				},
                //第三方库
				vendor: {
					test: /node_modules/,
					chunks: "initial",
					name: "vendor",
					priority: 10,
					enforce: true
				}
			}
		}
	},
    module:{
        rules:[
            {
                test:/\.js$/,
                use:{
                    loader:'babel-loader',
                    options:{ 
                        //TreeSharking 优化的语法来自ES6（webpack是先做生态，再做优化：到处抄袭 rollup）
                        //预编译：将ES6编译为ES5，设置为false 
                        presets:[
                            ["env",{modules:false}]
                        ]
                    }
                },
                include:path.resolve('./src'),
                exclude:/node_modules/
            },
            {
                test:/\.css$/,
                loader:['style-loader','css-loader']
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template:'./src/index.html'
        }),
        //开启变量提升 Scope Hoisting
        new ModuleConcatenationPlugin(),
        new webpack.HotModuleReplacementPlugin(),   //模块热更新插件
        new webpack.NamedModulesPlugin(),    //用名称代替ID
    ]
}