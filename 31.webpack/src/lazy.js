//懒加载：一串pipe的执行流程序列，preload配置好；执行时并列加载资源就绪 ——> 异步编程的思想
// bigpipe + chunk传输
// 视频流点击播放 

//Co + Promise
//async (generate迭代器的自动执行器) 非阻塞 yield让出本次CPU时间片，执行的是一段子函数(复合函数中)：闭包作用域链上的一个子函数callStack + await Promise 
// Node.js：一个基于V8(虚拟机引擎)的JS运行时环境，底层EventLoop(单线程)的实现是异步io库libuv。  单线程方式分配多个微任务(协程)
// python的协程也是单线程方式

// golang runtime / javaJRE 基于JVM 实现的多线程模型：CPU抢占式调度 
// 多线程方式分配 goroutine (m << n) ——> GMP模型 
document.getElementById('play').addEventListener('click',function(){
    //import 异步 加载 模块是一个es7的语法
    //在webpack里import是一个天然的分割点：import引入依赖的子模块 
    //import 对应webpack源码实现：webpackJsonpCallback(data)
    import('./video.js').then(function(video){
        let name = video.getName();
        console.log(name);
    });
    import('./video.js').then(function(video){
        let name = video.getName();
        console.log(name);
    });
});