import name from './h'; //var h = ('hello'); 
console.log(name);
// 开启变量提升 Scope Hoisting,这是一种优化手段
//作用域链查找，减少作用域和变量声明（函数、变量）var function 
//内联模块 内联函数：优化函数，由2个变成了1个。 
// 临时数据栈（存放表达式的值）：闭包自执行函数  lambda表达式 > var表达式 增加了var作用域 > function 增加了callStack
// webpack --display-optimization-bailout

// () 等价于空函数，临时数据栈（存放表达式的值）——> 闭包对象 Closure{}
// lambda表达式  (function(i){i++})(i)  匿名函数自执行 

// C++的内联函数
// php变量的 copy-on-write 