let base = require('./base');
let base2 = require('./base2');
document.querySelector('#app').innerHTML = base();
document.querySelector('#app2').innerHTML = base2();
require('./index.css');

if(module.hot){
    // 如果检测到了base模块更新了(事件)，则会找它所在的组件(绑定的DOM)处理，调用此回调函数
    // 事件冒泡机制：交给上级组件处理，层层捕获；如果不处理就继续传递给上层，直到最终的浏览器window刷新页面。
    // css-loader style-loader   内置实现了module.hot.accept
  module.hot.accept('./base',function(){
    let base = require('./base');
    document.querySelector('#app').innerHTML = base();
  });
}