// 受控组件 和非受控组件 (state)
// 指的都是表单元素Form：<input>  <textarea>  <select>
//典型的内部维护自己的 state：根据用户的输入进行更新，只能使用 setState()进行更新 
//输入框与state对象，合并为同一个真实的源，React将输入框的值进行传递给state对象; 由state对象来控制输入框的显示 

//本质上，受控组件的state对象，就是输入框元素的绑定代理
//  Element的value = this.state.value 
//  通过onChange事件绑定输入框，事件回调函数操作this.state对象，更新setState() ——> 实现输入框的值更新 
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class App extends Component {
    constructor() {
        super();
        this.state = { a: 'zf', b: 'zfpx' }
        // textarea value的初始化在state对象中初始化
        this.state = { value: 'please write an esaay about your favorite DOM element.' };
    }
    handleSubmit = (e) => {
        e.preventDefault(); // 指的就是提交表单事件
    }
    //处理元素值更新：也就是控制state值对象的变化
    handleChange = (e) => {
        let name = e.target.name
        // ES6语法：[name] 计算属性名，解析为变量name的值 
        this.setState({ [name]: e.target.value });
    }

    //返回且只能返回一个顶级元素
    // form filed 提供了一个value的props对象，必须还指定onChange事件的回调函数。否则就是一个只读的field
    render() {
        // 通过ajax 传递数据
        return (<form onSubmit={this.handleSubmit}>
            {/*  双向数据绑定 */}
            {/* react默认先将状态绑定到视图上 状态不变视图就不会刷新 */}
            <input type="text"
                required={true}
                value={this.state.a}
                onChange={this.handleChange}
                name="a"
            /> {this.state.a}
            <input type="text"
                required={true}
                value={this.state.b}
                onChange={this.handleChange}
                name="b"
            />{this.state.b}
            <textarea value={this.state.value} onChange={this.handleChange} />
            <select value={this.state.value} onChange={this.handleChange}>
                <option value="Grapefruit"> Grapefruit </option>
                <option value="lime"> Lime </option>
                <option value="Coconut"> Coconut(椰子) </option>
            </select>
            <input type="submit" />
        </form>)
    }
}

// 受控组件：表单元素的值，受组件的状态控制。
// 受控组件 可以对输入进行监控,而且可以对表单进行默认值操作
ReactDOM.render(<App></App>, window.root) 
