
npm install -g create-react-app
create-react-app react-dom-diff 

## 路由 react-router-dom 

## DOM diff  区分的keys减少节点重建  
DOM节点操作比较慢：回流、重绘(gpu)，都是很占用CPU和内存资源的。
但是虚拟DOM：vdom 模拟真实DOM树的数据结构，js对象，操作非常快。
vdom 转换成真实DOM，setState()更新state对象，并插入到页面中，完成渲染。
domdiff算法分析vdom的改变(事件触发) ——> Patch对象 ——> Patch对象应用更新到真正的DOM树上

比较2个DOM树的差异，是vdom算法最核心的部分，DOM DIFF 算法有3个优化策略：
- DOM节点的跨层级的移动操作，特别少，可以忽略不计。大多情况是：GPV祖孙三代 
    如果有跨层，多余的节点会转移到下一代/层
- 拥有相同类的两个组件会生成相似的树形结构，拥有不同类的组件会生成不同的树形结构。
    如果2个DOM树结构完全变了，最好的方式是删除重建，而不是打补丁(结构已经乱了没必要了)
- 同一层级的节点，局部重排的key diff：叶子节点的三种操作  insert move remove 
    key diff 为什么不用动态规划中的最小编辑距离算法？O(m*n)太高了

## 状态管理 redux(发布订阅到 store：数据中心)  mobx
redux + react-redux + redux-promise + redux-thunk + redux-logger
redux-saga（比redux-thunk强大得多，重点掌握）
dva + antdesign + react-router-redux 

 + 珠峰课堂 

## 中间件 middleWare

## react设计思想与源码分析，自己写一个react核心功能 

## react UI库
常用的：表单、图片裁剪与预览、拖拽窗口变化 

## 浏览器渲染原理 

## 前端数据监控

## 项目实战
服务器部署：nginx MySQL Linux docker部署 

## 复杂组件的单元测试、UI测试

