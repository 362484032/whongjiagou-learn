function add1(str) {
	return str + 1;
}
function add2(str) {
	return str + 2;
}
function add3(str) {
	return str + 3;
}
//let r = add3(add2(add1('zfpx')));
//console.log(r);//zfpx123

/**
 * compose(middileWareArr) 组合中间件 柯里化高阶组件
 *    AOP 装饰器模式
 */
//redux 3.4 就是这么实现的 compose
function compose1(...fns) {
	return function (...args) {
      if (fns.length === 0) {
         return args[0]
      }
		let last = fns.pop();
		return fns.reduceRight((composed, fn) => {
			return fn(composed);
		}, last(...args));
	}
}

//后来优化后的 compose ——> 迭代递推(first+rest)：分治思想 
// pipeline 迭代器：每次执行第一个，将结果返回(表达式)传递给 next
// 1、类比数学中的递推数列  递推表达式：迭代，分治(必须满足最终的next是有解的：终止条件退出递推) 
// 2、参数分解：满足乘法结合律 
function compose(...fns) {
   if (fns.length === 0) {
      return args[0]
   }
	if (fns.length === 1) return fns[0];
	return fns.reduce(
		(a, b) => (...args) => a(b(...args))
	);
}

let add = compose(add3, add2, add1);
let r = add("zfpx");
console.log(r); // zfpx123
