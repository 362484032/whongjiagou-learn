import * as types from './action-types';
/**
 * action-types 
 *   saga 处理异步 action(微任务)
 */
export default {
    increment(){
        return {type:types.INCREMENT_ASYNC};
    }
}