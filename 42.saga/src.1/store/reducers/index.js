import * as types from '../action-types';
/**
 * reducers 处理同步 action(宏任务)
 * @param {Object} state 
 * @param {Object} action 
 * @returns newSatee
 */
export default function (state = { number: 0 }, action) {
    switch (action.type) {
        case types.INCREMENT:
            return { number: state.number + 1 };
        default:
            return state;
    }
}