import {createStore,applyMiddleware,compose} from 'redux';
import reducers from './reducers';
import createSagaMiddleware  from 'redux-saga';
import {rootSaga} from '../sagas';
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'
import createHistory from 'history/createHashHistory'
let history = createHistory();
let middlewareRouter = routerMiddleware(history);
// composeEnhancers 组合中间件
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//这是一个可以帮你运行saga的中间件：generate函数的自动执行器(迭代器)
let sagaMiddleware = createSagaMiddleware();
//let store = applyMiddleware()(createStore)(reducers);
let store = createStore(reducers,composeEnhancers(applyMiddleware(sagaMiddleware,middlewareRouter)));

//通过中间件执行或者说运行saga；
// rootSaga监听所有的action（微任务队列，非阻塞执行）利用CPU多核
sagaMiddleware.run(rootSaga, store);
window.store = store;
export default store; 