//JS解析器：esprima 
//npm i  esprima estraverse escodegen -S
// 在线工具：astexplorer
const esprima = require('esprima');
const estraverse = require('estraverse');
const escodegen = require('escodegen');

const code = 'function ast(){}';
//1. esprima 分词(词法分析)：将代码段解析为AST（token分词）
const ast = esprima.parse(code);

//2. estraverse 语法分析(语义): scanner遍历AST(token分词)，做语法分析并推断优化
estraverse.traverse(ast, {
    enter(node) {
        console.log('enter ', node.type);
        if (node.type == 'Identifier') {
            node.name += '_enter';
        }

    },
    leave(node) {
        console.log('leave ', node.type);
        if (node.type == 'Identifier') {
            node.name += '_leave';
        }
    }
});

//3. escodegen 生成语法优化后的代码 
let result = escodegen.generate(ast);
console.log(result);//重新生成后的代码
