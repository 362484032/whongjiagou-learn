# 抽象语法树(Abstract Syntax Tree)

webpack 和 Lint 很多的工具库的核心都是通过抽象语法树(Abstract Syntax Tree) 这个概念来实现对代码语法的检查、变量类型推断和表达式优化分析等编译器在编译期间要完成的工作：编译器的编译阶段：词法分析(Token)、语法分析

通过了解 AST，你可以随手编写类似的工具。

## 抽象语法树的用途

- 代码语法的检查、代码风格的检查、代码格式化、代码高亮、代码错误提示、代码自动补全等
  - 比如 JSLint、JSHint 对代码错误或风格的检查，发现一些潜在的错误
  - IDE的错误提示、代码格式化、高亮、自动补全
- 代码混淆压缩：换行、回车、空格
  - UglifyJS2
- 优化变更代码、优化代码结构 ——> 代码优化与重构
  - 代码打包工具 webpack、rollup等
  - CommonJS（node.js）、AMD（require.js）、CMD（sea.js）、UMD（es6Module）等代码规范之间的转化 
    - AMD（require.js）、CMD（sea.js）已经废弃不用了
  - CoffeeScript、TypeScript、JSX（js+xml）等转化为原生JavaScript 

## 抽象语法树的定义

![AST定义](./assets/AST定义.png)

### AST 在编译中的位置

在编译原理中，编译器转换代码通常要经过三个步骤：词法分析（Lexical Analysis）、语法分析（Syntax Analysis）、代码生成（Code Generation），下图生动展示了这一过程：





