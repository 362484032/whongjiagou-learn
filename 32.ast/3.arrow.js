/**
 * babel-core  babel核心库，用来实现AST的不同语法格式的转换引擎 
 *   AST语法分析，转换不同的语法格式
 */
const babel= require('babel-core');
//可以实现类型判断，生成AST节点
const types = require('babel-types');

let code = `let sum = (a,b) => a+b`; // let sum = function (a,b){return a+b}
//访问者模式Vistor：对于某个对象或一组对象，不同的访问者，产生的结果不同，执行的操作也不同。
//浏览器渲染也会用到
// 这个访问者可以对特定的类型的节点进行处理
const visitor = {
  ArrowFunctionExpression(path){
    let params = path.node.params;
    let blockStatement = types.blockStatement([
        types.returnStatement(path.node.body)
    ]);
    let func = types.functionExpression(null, params, blockStatement, false, false);
    path.replaceWith(func);
  }
}

let arrayPlugin = {visitor}
//babel内部先先把代码转成AST,然后进行遍历，
let result = babel.transform(code,{
    plugins:[
        arrayPlugin
    ]
})
console.log(result.code);

