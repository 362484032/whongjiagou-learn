## 聊天室：socket.io + dva + egg.js

### dva 

dva全家桶：react react-dom redux redux-saga react-router react-router-dom history 

基于 redux redux-saga react-router 的轻量级前端框架
- react-router-dom 基于react-router，对浏览器history api 进行了扩展 BrowserRouter和HashRouter；主要用于管理页面路由 Component(view/jsx)
- Controller 通过 store.dispatch(action)调度 
- Model：redux redux-saga 的 reducers 来处理 

roadhog 打包工具，webpack的简化版 