/**
 * 组合handler：纯函数 reducers下的Controller 
 *   Controller名与组件名保持一致
 */
export default reducers => (state = {}, action) => 
Object.keys(reducers).reduce(
    (currentState, key) => {
        currentState[key] = reducers[key](state[key], action);
        return currentState;
    }, {});