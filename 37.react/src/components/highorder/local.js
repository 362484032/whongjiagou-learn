import React,{Component} from 'react';
/**
 * 高阶组件就是一个函数，用来封装重复的逻辑。
 *  传进去一个老组件，返回一个增强版(逻辑复用)的新组件
 * 
 * @param {Class}  OldComponent 组件类
 * @param {String} name         组件名
 * @param {String} placeholder  占位符(默认输入框)
 * @returns Class 
 */
export default function(OldComponent, name, placeholder){
    class NewComponent extends Component {
        constructor(){
            super();
            this.state = {data:''};
        }  
        componentWillMount(){
            this.setState({ data: localStorage.getItem(name) || placeholder });
        }
        save = (event) => {
            localStorage.setItem(name, event.target.value);
        }
        render(){
            return <OldComponent data={this.state.data} save={this.save} />
        }
    }
    return NewComponent;
}