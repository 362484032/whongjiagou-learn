import React, { Component } from 'react';
import ReactDOM from 'react-dom';
/**
 * lifeCycle 组件生命周期
 * 
 * 父子组件
 *   当子组件收到父组件初始化新的属性值时
 */
class SubCounter extends Component {
    //当子组件将要接收到父组件传给它的新属性的时候
    componentWillReceiveProps() {
        console.log('SubCounter componentWillReceiveProps');
    }
    //Returned undefined instead of a boolean value. Make sure to return true or false.
    shouldComponentUpdate(nextProps, nextState) {
        console.log('SubCounter shouldComponentUpdate');
        if (nextProps.count <= 5) {
            return true;
        } else {
            return false;
        }
    }
    componentWillUpdate() {
        console.log('SubCounter componentWillUpdate');
    }
    componentDidUpdate() {
        console.log('SubCounter componentDidUpdate');
    }
    render() {
        return (
            <div>
                子计数:{this.props.count}
            </div>
        )
    }
}

/**
 * 组件是一个类，当它被使用的时候实例化，然后挂载到页面中
 * 
 */
export default class Counter extends Component {
    //默认属性对象(类共有)
    static defaultProps = {
        count: 0
    }
    //构造函数：初始化对象属性(内部私有)
    constructor(props) {
        console.log('constructor');
        super(props);
        this.state = { count: props.count };//初始化默认状态对象
    }
    componentWillMount() {
        console.log('1.组件将要挂载 componentWillMount');
    }
    handleClick = () => {
        this.setState(prevState => ({
            count: prevState.count + 1
        }));
    }
    //询问组件是否要被更新,当一个组件的属性或者状态只要有一个发生了改变 ，默认就会重新渲染
    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.count < 10) {
            return true;
        } else {
            return false;
        }
    }
    componentWillUpdate() {
        console.log('componentWillUpdate');
    }
    componentDidUpdate() {
        console.log('componentDidUpdate');
    }
    componentWillUnmount() {
        console.log('componentWillUnmount');
    }
    //卸载组件（清掉定时器）
    destroy = () => {
        window.clearInterval(this.timer);
        ReactDOM.unmountComponentAtNode(document.querySelector('#root'));
    }
    render() {
        console.log('2.render 挂载');
        return (
            <div>
                父计数器:{this.state.count}
                <button onClick={this.handleClick}>+</button>
                <button onClick={this.destroy}>destroy</button>
                <SubCounter count={this.state.count} />
            </div>
        )
    }
    //组件挂载完成(插入到页面)：加入定时器更新state，通知页面渲染
    componentDidMount() {
        console.log('3.componentDidMount 组件挂载完毕');
        //Can't call setState (or forceUpdate) on an unmounted component
        //This is a no-op, but it indicates a memory leak in your application
        this.timer = window.setInterval(() => {
            this.setState((prevState, props = {'increment':1}) => ({
                count: prevState.count + props.increment
            }));
        }, 1000);
    }
}

