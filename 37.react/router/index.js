import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
// 测试驱动开发 先写一个测试用例，看看原生路由的效果，然后自己实现核心功能

//HashRouter 通过路径 里的哈希变量实现的
//BrowserRouter  用的是 html5的history API实现 
// import {HashRouter as Router,Route,Switch} from 'react-router-dom';
import {HashRouter as Router,Route,Switch} from './react-router-dom';
import App from './components/App';
import User from './components/User';
import Protected from './components/Protected';
import Login from './components/Login';

/**
 * Router 是路由容器，管理Route并提供通过path查找Route；路由表中记录了所有Route规则 
 *   Router 通过context 传递给子组件Route
 *   Route 代表一条的路由规则 
 */
let Home = (props, context)=>{
  console.log('props', props, 'context', context)
    return <div>首页</div>
}
let Profile = ()=><div>个人设置</div>
// if elif elif else   if else 
//渲染的时候会先取当前的路径(location.hash),然后跟path进行匹配，如果能匹配上则显示component指定的组件，如果不能匹配，则不显示
// ReactDOM.render(
//        <App>
//          <Switch>
//           <Route  path="/home" component={Home}/>
//           <Route path="/user" component={User}/>
//           <Route path="/login" component={Login}/>
//           <Protected path="/profile" component={Profile}/>
//          </Switch>
//        </App>,
//     document.querySelector('#root')
// );

//跑通路由
ReactDOM.render(
    <App>
      <Router>
        <Switch>
        <Route  path="/home" component={Home}/>
        <Route path="/user" component={User}/>
        <Route path="/login" component={Login}/>
        <Protected path="/profile" component={Profile}/>
      </Switch>  
      
      </Router>
    </App>,
  document.querySelector('#root')
);
/**
context：
  {
    history:{
      push()
    },
    location:{pathname:'/home'},
    match{
      params:{},
      path:'/home',
      url:'/home'
    }
  }
  url /user/datail/1
  path /user/datail/:id
  params= {}
 */