import React,{Component} from 'react';
import {bindActionCreators} from '../redux';
import propTypes from 'prop-types';
/**
 * ProxyComponent  Component组件代理 
 *  1、实现state数据转换：组件state <=> Store.dispatch
 *  2、转发组件请求/指令 actions(就是Promise，pipeline中间件链)：
 *    bindActionCreators(actions, store.dispatch)
 * 
 * @param {stateConvertMap} mapStateToProps 
 * @param {actions} mapDispatchToProps 
 * @returns 
 */
export default function(mapStateToProps, mapDispatchToProps){
   return function(WrapedComponent){
      class ProxyComponent extends Component{
          static contextTypes = {
              store:propTypes.object
          }
          constructor(props,context){
            super(props,context);
            this.store = context.store;
            this.state = mapStateToProps(this.store.getState());
          }
          componentWillMount(){
              this.unsubscribe = this.store.subscribe(()=>{
                  this.setState(mapStateToProps(this.store.getState()));
              });
          }
          componentWillUnmount(){
              this.unsubscribe();
          }
          render(){
              let actions= {};
              if(typeof mapDispatchToProps == 'function'){
                actions = mapDispatchToProps(this.store.disaptch);
              }else if(typeof mapDispatchToProps == 'object'){
                actions = bindActionCreators(mapDispatchToProps, this.store.dispatch);
              }
                return <WrapedComponent {...this.state} {...actions}/>
         }
      }
      return ProxyComponent;
   }
}