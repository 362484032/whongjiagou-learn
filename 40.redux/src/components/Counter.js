import React, { Component } from 'react';
import store from '../store';
import actions from '../store/actions/counter';
import {connect} from '../react-redux';
//redux.bindActionCreators 用来实现actionCreator和Store.dispatch的绑定
// react-redux/connect 中调用此方法
// let newActions = bindActionCreators(actions, store.dispatch);

// action.increment    dispatch(action.increment());
class Counter extends Component {
    constructor(props){
        super(props);
        this.state = {number:props.number};
    }
    //subscribe：挂载组件到DOM节点，setState()触发渲染页面
    componentWillMount(){
        this.unsubscribe = store.subscribe(()=>{
            this.setState({number:this.props.number});
        });
    }
    //unsubscribe：卸载组件
    componentWillUnmount(){
        this.unsubscribe();//取消订阅
    }
    render() {
        return (
            <div style={{border:'1px solid red'}}>
                <p>{this.state.number}</p>
                <button onClick={this.props.increment} >+</button>
                <button onClick={this.props.decrement}>-</button>
                <button onClick={()=>{
                    setTimeout(()=>{
                        this.props.increment()
                    },1000);
                }}>过一秒后再加</button>
            </div>
        )
    }
}
//connect是一个高阶组件函数
// 把仓库中的状态树映射为当前组件的属性对象
// 负责输入，就是把仓库中的状态输入到组件，
let mapStateToProps = state => state.counter;

//把store的dispatch方法转换成一个当前组件的属性对象
// 输出 把用户在组件中的操作发射出去
//1. 自绑定
let mapDispatchToProps = dispatch = ({
   increment:()=>dispatch(action.increment)
})
//2. 直接把actionCreators放在这。内部实现自绑定
//let mapDispatchToProps = actions;

export default connect(
    state=>state.counter,
    actions
)(Counter);