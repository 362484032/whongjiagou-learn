export default function bindActionCreators(actions,dispatch) {
    let newActions = {};
    for(let attr in actions){
        newActions[attr] = function(){ 
            // arguments 不定参数,专门为函数闭包而设计
            // Closure 对象中的私有属性
            // nodeJs中 arguments = 输入的实参 ?? 形参的默认值 ['exports', 'require', 'module', '__filename', '__dirname']
            dispatch(actions[attr].apply(null, arguments));
        }
    }
    return newActions;
 }

 fn = new Function('exports', 'require', 'module', '__filename', '__dirname', content + '\n return module.exports;');