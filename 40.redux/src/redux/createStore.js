export default function (reducer, preloadedState) {
    let state = preloadedState;
    let listeners = [];
    function getState() {
        // return state;
        return JSON.parse(JSON.stringify(state));
    }
    /**
     * dispatch 派发分发 组件的操作行为action 
     * 
     * @param {function} action 
     */
    //Store将一个操作$action传递给 Reducers：(state,action) 
    //action 动作/指令，描述组件的行为；action是一个普通的JS对象，只有一个属性是必须的 type，其它属性随意。
    function dispatch(action) {
        //接收新的动作后，通过 才状态 和新动作计算出新状态
        state = reducer(state, action);
        //组件订阅subscribe：挂载组件期间会setState()，触发通知更新DOM页面，即执行所有的监听函数 render()
        listeners.forEach(listener => listener());
    }
    //派发了一个动作获取初始值，其实在redux内部是派发一个INIT: '@@redux/INIT'动作
    dispatch({ type: '@@redux/INIT' });
    //挂载View的组件时，订阅Store容器中state的变化；
    // 如果组件操作Action执行更新了state，会通知View组件的逻辑 (更新View: render)
    function subscribe(listener) {
        listeners.push(listener);
        // 返回一个取消订阅函数（View组件卸载，回收资源：render渲染页面完成后）
        return function unsubscribe() {
            listeners = listeners.filter(item => item != listener)
        }
    }
    return {
        getState, dispatch, subscribe
    }
}